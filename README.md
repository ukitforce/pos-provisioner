# How to Provision a POS machine (+Printer) - manually:

## Steps:

### Install Fedora (or a Debian derivative) on the POS machine:
You can use a Fedora (GNOME) Live USB disk to boot and install Fedora on the machine. The automation of this installation can be done using kickstart system, covered separately in [kickstart.md](kickstart.md).

Download the fedora live USB from [getfedora.org](getfedora.org) on a Linux computer, and burn it / write it on the USB using the special command:

```
dd if=/home/waqar/Downloads/Fedora-Live-36.iso  of=/dev/sdx bs=8M
```

Note: Adjust the path/name of the ISO file, and also the actual device name for your USB before executing the command above. Normally the command `fdisk -l` shows the list of storage devices attached to the computer.


### Hardware:
* Connect printer with the POS machine over USB cable. Some printers are built into the main POS machine, connected internally to USB port.
* Make a note if the printer is POS80 or POS50 or something else.
* Install a Wireless network adaptor, on USB port.
* If the POS machine has only one USB port, then you will need to add a small USB Hub on this port, and then connect other devices to this USB Hub. Make sure to attach the USB hub securely to the POS - with glue or valcro, etc - so it does not come off easily.

### Use the following options during the installation:
* Keyboard (English/GB)
* TimeZone (London)
* Disk partitioning (Automatic - destroy all existing partitions)

### Post installation:
* Create a user named `pos`, setup a secure password for it (and make a note for it), and select correct keyboard layout for the user. This user will have sudo privileges. (You can change this later if you want.)
* Select hostname as `till-<number>.<branch>.customer-fqdn`
* Configure the user to login automatically at boot time in `Settings`
* Connect to the correct wireless network
* Copy in your public keys into the user `pos` home directory
* Setup SSH service to start at boot time
* Setup CUPS printing service and configure printer
* Setup firefox properties
* Download and install Chrome, and configure it as required


## Post-installation detailed steps:

### Setup SSH keys in the user `pos`:
While logged in as user `pos`, download the public keys of the admin team and put those keys in `/home/pos/.ssh/authorized_keys` file. Follow these steps:


(as user `pos`)

```
cd /home/pos

mkdir .ssh
chmod 0700 .ssh

wget -O /home/pos/.ssh/authorized_keys \
  https://gitlab.com/ukitforce/public-ssh-keys/-/raw/main/authorized_keys


# Verify:
cat  /home/pos/.ssh/authorized_keys
```


### Setup SSH keys in the user `root`:

Switch to user `root`, and download the public keys of the admin team and put those keys in `/root/.ssh/authorized_keys` file. Follow these steps:

```
sudo -i

cd /root

mkdir .ssh
chmod 0700 .ssh

wget -O /root/.ssh/authorized_keys \
  https://gitlab.com/ukitforce/public-ssh-keys/-/raw/main/authorized_keys


# Verify:
cat  /root/.ssh/authorized_keys
```


### Enable basic services:

```
firewall-cmd --zone=public --add-service=ssh

systemctl enable sshd 
systemctl start sshd 

yum -y install cups crontabs cronie nano nano-default-editor


systemctl enable cups
systemctl start cups
```


### Disable Fedora background auto updates and update/upgrade alerts, etc:

(as root)
```
systemctl stop packagekit.service

systemctl disable packagekit.service

systemctl mask packagekit.service

systemctl stop packagekit-offline-update.service

systemctl disable packagekit-offline-update.service

systemctl mask packgekit-offline-update.service
```

### Set computer/hostname:

(as root)

```
vi /etc/hostname

till-1.branch.jjpp.co.uk
```

### Update system software:

You can always **update** system software for newer minor version upgrades, like bug fixes, etc - using `yum update`. Please note that this will **not** upgrade the system to newer version of fedora. 

(as root)
```
yum -y update
```

To **upgrade**, there is a separate command `yum upgrade` , which is a more involved/complex process, and we don't need to do it.



### Configure CUPS:

In the `/etc/cups/cups-files.conf` file, add group `pos` to `SystemGroup` .

(run as user `root`)

Manual method:
```
sudo -i

# vi /etc/cups/cups-files.conf
. . . 

SystemGroup sys root wheel pos

. . . 
```

Automatic method:
(as root)
```
sed -e 's/\(^SystemGroup.*\)/\1\ pos/g' -i /etc/cups/cups-files.conf 
```

Verify:
```
# grep SystemGroup  /etc/cups/cups-files.conf

SystemGroup sys root wheel pos 
```

Restart cups:

```
systemctl restart cups
```


### Download POS-80 (or POS50) printer drivers:

Please confirm the type of printer you want to use. i.e. `POS80` or `POS50`. Adjust the lines below for the correct driver file.

(run these commands as root:)

```
sudo -i

DRIVER_URL=https://gitlab.com/ukitforce/pos-printers/-/raw/master/drivers/posdrv_linux

HOME_DIR=/home/pos/

wget -q -O /usr/lib/cups/filter/rastertopos ${DRIVER_URL}/filter/x86_64/rastertopos

wget -q -O ${HOME_DIR}/rastertopos ${DRIVER_URL}/filter/x86_64/rastertopos

wget -q -O ${HOME_DIR}/pos80.ppd ${DRIVER_URL}/ppd/pos80.ppd

wget -q -O ${HOME_DIR}/pos58.ppd ${DRIVER_URL}/ppd/pos58.ppd

chown pos:pos ${HOME_DIR}/*.ppd 

chmod +x /usr/lib/cups/filter/rastertopos

```


### Setup printer in CUPS:

As user `pos`, open cups WEB page in a browser using [http://localhost:631](http://localhost:631) . Since user `pos` is a member of CUPS `SystemGroup`, you should be able to run the `Add printer` wizard while logged in as user `pos` . You may be required to provide the password for user `pos` when CUPS asks for it.

Add printer POS80 or POS50 (POS58). The PPD file is stored in `/home/pos/pos80.ppd`  and `/home/pos/pos58.ppd`

| ![https://gitlab.com/ukitforce/pos-printers/-/raw/master/images/cups-pos-printer-1.png](https://gitlab.com/ukitforce/pos-printers/-/raw/master/images/cups-pos-printer-1.png) |
|:------:|

| ![https://gitlab.com/ukitforce/pos-printers/-/raw/master/images/cups-pos-printer-2.png](https://gitlab.com/ukitforce/pos-printers/-/raw/master/images/cups-pos-printer-2.png) |
|:------:|

**Note:** set 300mm height for the printer settings in CUPS, ignore the snapshot above.

Adjust margins, paper size, etc, and print a test page.

### Printer settings in chrome for devices to print order in kitchen:
If you don't see the printer click more in printers list to select POS printer
* Paper size: 72mmx300mm
* Margins: Custom (2mm on all 4 sides)
* Scale: Customised: 95
* Switch off headers and footers

**Note:** Once this setting is used, it will be saved for future in chrome. Please note this setting is NOT for POS system.

### Printer settings in chrome for POS systems to print order on counter:
If you don't see the printer click more in printers list to select POS printer
* Paper size: 72mmx300mm
* Margins: Custom (4mm top and bottom, 0 left and right)
* Scale: Customised: 88
* Switch off headers and footers

**Note:** Once this setting is used, it will be saved for future in chrome. Please note this setting is NOT for devices.


### Install script to check printer every few minutes:
The printer sometimes becomes offline for no reason. Therefore, we created a small BASH script, which checks the printer every few minutes, and restarts it if it finds it offline. The script is maintained in a separate repository named `pos-printers`. Since the repository is public repository, we can pull the script from it directly and setup cron to run it every few minutes.


(as root:)
```
wget -q -O /root/check-printer.sh \
  https://gitlab.com/ukitforce/pos-printers/-/raw/master/scripts/check-printer.sh
  
chmod +x /root/check-printer.sh
```

Next, edit crontab, and add a line to run this script **every minute**. The other line is also important, as it will remove old log files and prevent disk from becoming full of logs.

(as root:)

```
crontab -e

*/1 * * * * /root/check-printer.sh

0 0 */1 * * /usr/bin/journalctl --vacuum-time=30d
```

Verify using:

```
[root@fedora ~]# crontab -l
*/1 * * * * /root/check-printer.sh

0 0 */1 * * /usr/bin/journalctl --vacuum-time=30d
[root@fedora ~]# 
```

Watch the `journalctl` logs, and check if check-printer.sh is generating messages every minute.


```
journalctl -xef | grep -i printer
```

You should see output like this:

```
Nov 27 18:22:02 till-3.branch2.jojosperiperi.co.uk root[24941]: PRINTER_STATUS is: printer POS is idle. enabled since Sat 26 Nov 2022 20:18:07 GMT
Nov 27 18:22:02 till-3.branch2.jojosperiperi.co.uk CROND[24899]: (root) CMDOUT (Printer - POS80 - found enabled. ALL OK. Nothing to do.)
Nov 27 18:22:02 till-3.branch2.jojosperiperi.co.uk root[24943]: Printer - POS80 - found enabled. ALL OK. Nothing to do.
Nov 27 18:22:02 till-3.branch2.jojosperiperi.co.uk CROND[24899]: (root) CMDEND (/root/check-printer.sh)
Nov 27 18:23:02 till-3.branch2.jojosperiperi.co.uk CROND[25016]: (root) CMD (/root/check-printer.sh)
Nov 27 18:23:03 till-3.branch2.jojosperiperi.co.uk CROND[24990]: (root) CMDOUT (PRINTER_STATUS is: printer POS is idle. enabled since Sat 26 Nov 2022 20:18:07 GMT)
Nov 27 18:23:03 till-3.branch2.jojosperiperi.co.uk root[25031]: PRINTER_STATUS is: printer POS is idle. enabled since Sat 26 Nov 2022 20:18:07 GMT
Nov 27 18:23:03 till-3.branch2.jojosperiperi.co.uk CROND[24990]: (root) CMDOUT (Printer - POS80 - found enabled. ALL OK. Nothing to do.)
Nov 27 18:23:03 till-3.branch2.jojosperiperi.co.uk root[25033]: Printer - POS80 - found enabled. ALL OK. Nothing to do.
Nov 27 18:23:03 till-3.branch2.jojosperiperi.co.uk CROND[24990]: (root) CMDEND (/root/check-printer.sh)
```


(ctrl+c to break)


 

### Setup Firefox: (only for POS machines)

Run these steps as user `root`:

```
sudo -i


# Also add firefox-kiosk to auto start using gnome-tweaks (or through CLI)
# Copy the "firefox-kiosk.desktop" file additionally in /home/pos/.config/autostart/

# Adjust name of user (pos):
mkdir -p /home/pos/.config/autostart

wget -q -O /home/pos/.config/autostart/firefox-kiosk.desktop \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/firefox-kiosk.desktop

# Adjust name of user (pos):
chown -R pos:pos /home/pos/.config
```

**Note:** Adjust/edit the (.desktop) files, to adjust the Web URL required to open automatically.

### Setup Google chrome:

Install Google chrome from internet - manually. Download RPM file, and install it using:

```
yum -y localinstall google-chrome.rpm
```

After installation is finished. Start it manually, then close it again.


After google-chrome is closed, copy the `google-chrome.desktop` file to the user's autostart location, so google-chrome can start at system start time.

```

# Adjust name of user (pos):
mkdir -p /home/pos/.config/autostart

wget -q -O /home/pos/.config/autostart/google-chrome.desktop \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/google-chrome.desktop

# Adjust name of user (pos):
chown -R pos:pos /home/pos/.config
```

### Printing preferences of Google-Chrome:
Google chrome preferences are saved in `/home/pos/.config/google-chrome/Default/Preferences` file, which is in json format. It contains printing options, such as margins, scaling, etc.

```
grep -w scaling  /home/pos/.config/google-chrome/Default/Preferences
```

This file is part of this repository, as `google-chrome-Preferences.json`. If you want to restore is to the running system, it has to be renamed as `Preferences` and placed inside `/home/pos/.config/google-chrome/Default/` directory.


### Disable Wayland:
It is important to disable `Wayland` and switch to the `XOrg` system for the graphical user interface for GNOME. Open `/etc/gdm/custom.conf` and un-comment the line:

(run as `root` :)

Manual method:

```
sudo -i

# vi /etc/gdm/custom.conf
. . . 
WaylandEnable=false
. . . 
```

Automatic method:

```
# sed -e  's/^\#WaylandEnable=false/WaylandEnable=false/g'   -i /etc/gdm/custom.conf 
```

Verify:

```
# grep WaylandEnable /etc/gdm/custom.conf

WaylandEnable=false
```




### Fix the login screen positioning problem:

On older POS systems (POS2 and POS3) the initial Fedora login screen shows up on wrong screen, which is customer facing instead of operator facing screen. To fix this, we need to adjust a file  `~/.config/monitors.xml` . This file is created when you use Fedora GNOME settings to assign correct screen as primary screen and the positioning etc.

(as root):

```
cp -v /home/pos/.config/monitors.xml /var/lib/gdm/.config/ 
chown gdm:gdm /var/lib/gdm/.config/monitors.xml
```

### Delete GNOME keyring:
This helps us get rid of the keyring authorization problem. After removing/disabling these files you will need to log out and login again. Afterwards the keyring will be created again, choose to give empty password and choose un-encrypted storage.

```
ls -lh /home/pos/.local/share/keyrings

mv /home/pos/.local/share/keyrings/login.keyring /home/pos/.local/share/keyrings/login.keyring.disabled
mv /home/pos/.local/share/keyrings/user.keystore /home/pos/.local/share/keyrings/user.keystore.disabled
```






------

# OPTIONAL SETTINGS

### Enable/setup angelfish browser (no need):

(as root) on Fedora 36
```
yum -y install angelfish

```

Create this file and put it in: 

```
rm -f /usr/share/applications/org.kde.angelfish.desktop


wget -q -O /home/pos/.config/autostart/angelfish-browser.desktop \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/angelfish-browser.desktop
```


```
[root@fedora ~]# cat > /home/pos/.config/angelfish/tabs.json << EOF
{
    "currentTab": 0,
    "tabs": [
        {
            "isDeveloperToolsOpen": false,
            "isMobile": false,
            "url": "https://pos.jojosperiperi.co.uk/"
        }
    ]
}
EOF

```

```
[root@fedora ~]# chmod 0440 /home/pos/.config/angelfish/tabs.json
```

### Setup Firefox:

Run these steps as user `root`:

```
sudo -i


# Also add firefox-kiosk to auto start using gnome-tweaks (or through CLI)
# Copy the "firefox-kiosk.desktop" file additionally in /home/pos/.config/autostart/

wget -q -O /usr/share/icons/firefox-kiosk.png \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/firefox-kiosk.png


# Adjust name of user (pos):
mkdir -p /home/pos/.config/autostart

wget -q -O /home/pos/.config/autostart/firefox-kiosk.desktop \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/firefox-kiosk.desktop

# Adjust name of user (pos):
chown -R pos:pos /home/pos/.config
```

**Note:** Adjust/edit the (.desktop) files, to adjust the Web URL required to open automatically.







### Set printer margins and other printer settigns, in firefox profile file:

**Note:** Do this only if you are using firefox as main browser for printing.

These are configured in the `user.js` file. Start `firefox`, and it will create a profile for itself the first time it runs. Find the path to `prefs.js` file, and copy your `user.js` file in that directory. Next time when firefox starts, it will merge `user.js` into `prefs.js` automatically. The [user.js](user.js) file is provided with this repository.

The name of the printer should be the same which you configured in CUPS.

Here is how to find where firefox's `prefs.js` is:

(as user `pos` :)
```
$ find . -name prefs.js

./.mozilla/firefox/vkm3mnuk.default-1588758113394/prefs.js
```

Now create `user.js`  in that directory, and fill it with correct values using the example `user.js` provided as a file in this repository.

```
$ vi ./.mozilla/firefox/vkm3mnuk.default-1588758113394/user.js

. . . 

```

### For Wifi hardware support for TPLink AC 600 (T2U Nano):

**Note:** This step is not needed if the Wifi is already working.

```
yum -y install gcc kernel-devel kernel-tools dkms wireless-tools
```


