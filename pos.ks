# Notes for this Kickstart to work correctly:
# ------------------------------------------
# * You mush have a local mirror of "Fedora-Everything" repository on 
#     a local computer / local network, served by a simple web server,
#     over any port, say 8080, 8888, 7979, etc. 
# * This mirror (mentioned above) will be about 200 GB in size. So watch out. 
#     One of the mirror is:
#     http://mirror.bytemark.co.uk/fedora/linux/releases/34/Everything/x86_64/os/
# * Use: "wget --mirror --no-parent  <URL>" to mirror a site.
# * Use "Fedora-Everything-netinst-x86_64-34-1.2.iso" file, burn it on a USB stick,
#    and use it as installer on the client (POS device) side.
# * Place this kickstart file on the network so it can be pulled by the installer.
# * Provide the kickstart URL to the installer by pressing <tab>, 
#   and using additional boot options:
#   * inst.ks=http://192.168.122.1/ks/pos.ks
#   * inst.repo=http://192.168.122.1/everything/
# * Boot options that are specific to the installation program always start with inst.
#   https://docs.fedoraproject.org/en-US/fedora/rawhide/install-guide/advanced/Boot_Options/



# Use graphical installer
# graphical

# Use text mode installer (faster)
text

# Below is equivalent of "inst.repo=http://192.168.122.1/dvd/" boot time option.
# url --url=http://192.168.122.1/dvd/


%packages

# @^basic-desktop-environment
# @admin-tools


# @Basic Desktop
# @Common NetworkManager Submodules
# @Core
# @Dial-up Networking Support
# @Fonts
# @Guest Desktop Agents
# @Hardware Support
# @Multimedia
# @Standard
# @base-x


# @hardware-support
ipw*
iwl*
bcm*
b43*
usb_mod*
zd1211*
alsa-*
atmel-*
acpi
acpitool

# To configure GNOME from CLI:
dconf-editor


NetworkManager-wifi


# @gnome-desktop
gdm
gnome-session

# necessary tools
gnome-terminal
gedit
firefox

# cups is not part of default linux installation (apparently)
-totem
%end

# Keyboard layouts
keyboard --xlayouts='no'
# System language
lang en_US.UTF-8

# System timezone
timezone Europe/London 

timesource --ntp-server 0.uk.pool.ntp.org
timesource --ntp-server 1.uk.pool.ntp.org
timesource --ntp-server 2.uk.pool.ntp.org
timesource --ntp-server 3.uk.pool.ntp.org


# Network information
network  --bootproto=dhcp --device=enp1s0 --noipv6 --activate
network  --hostname=001-ig11-001.cd.ukitforce.co.uk

# Firewall and SELinux:

firewall --enabled --ssh

# firewall --disabled

selinux --disabled


# Service to run at system boot: (comma separated list)
# (don't add cups here, as cups is not part of default package list).
services --enabled=sshd,gdm 


# Run the Setup Agent on first boot
# firstboot --enable
firstboot --disable

# Generated using Blivet version 3.3.0
ignoredisk --only-use=vda
autopart --type=plain  --fstype=ext4 

# Partition clearing information
# clearpart --none --initlabel
clearpart --drives=vda --all  --initlabel



# The actual passwords are stored as ENV variable in the repository CI/CD ENV Variables.

# Root password
rootpw --iscrypted $6$VcbD9aI6X4s8NPZW$yaJbN00BC0qMN/TN8nGHMy2djWag01xUWvFFgtqwCh1r.nDQ8SYaIrWaXeX5yzJmH76IaFedeyY9QBLu1ee9E1

# Note: User pos does not have admin rights. It is not a member of sudo.
#       This is intentional for security reasons.

user --name=pos --password=$6$O4zZfoVKGuwfoA9P$ZpCyJENLq1UYlQ9QOlYDH1ZmIX1w/tPV2sIeT//V9864A6uSsdwuVtEOlD7yJAalW5i1DAeV8hqwCj.clcsYT/ --iscrypted --gecos="pos"


# The ssh_key must be a full SSH key fingerprint, 
#   and it must be enclosed in quotes ("") because the key may contain spaces.

sshkey --username=root "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDROn5fovT1lZnJWPu3/cWuSjUyxh5pjme3JMvuWcdZg Kamran-ed25519-key"

sshkey --username=root "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBbJmq2gE5gkggB8WgsVDPufXpq4OYWwRwwHjYQKaZEU Waqar@WaqarDell"

sshkey --username=pos "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDROn5fovT1lZnJWPu3/cWuSjUyxh5pjme3JMvuWcdZg Kamran-ed25519-key"

sshkey --username=pos "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBbJmq2gE5gkggB8WgsVDPufXpq4OYWwRwwHjYQKaZEU Waqar@WaqarDell"


# ToDo: Add more public keys by using a script in the "post" section


# Shut down and power off the system after the installation has successfully completed.

poweroff



%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end



######################################################################## 

%post

#!/bin/bash

# Ensure that Gnome Display Manager is enabled, and default boot target is GUI:
# ----------------------------------------------------------------------------
systemctl enable gdm

systemctl set-default graphical.target


# Setup Firefox Desktop Icon and enable AutoStart for it:
# ------------------------------------------------------

# Download  "firefox-kiosk.desktop" file and place it in: /usr/share/applications/

# Also add firefox-kiosk to auto start using gnome-tweaks (or through CLI)
# Copy the "firefox-kiosk.desktop" file additionally in /home/pos/.config/autostart/

wget -q -O /usr/share/icons/firefox-kiosk.png \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/firefox-kiosk.png

# Adjust the Web URL in the files below:

wget -q -O /usr/share/applications/firefox-kiosk.desktop \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/firefox-kiosk.desktop

# Adjust name of user (pos):
mkdir -p /home/pos/.config/autostart

wget -q -O /home/pos/.config/autostart/firefox-kiosk.desktop \
  https://gitlab.com/ukitforce/pos-provisioner/-/raw/master/firefox-kiosk.desktop

# Adjust name of user (pos):
chown -R pos:pos /home/pos/.config/autostart  /home/pos/.config/dconf


# Install CUPS (Printing software/tools):
# --------------------------------------

# cups package is not included in the default installation for some stupid reason.

yum -y install cups
systemctl enable cups


# Download and install POS-80 printer driver:
# ------------------------------------------

wget -q -O /usr/lib/cups/filter/rastertopos \
  https://gitlab.com/ukitforce/pos-printers/-/raw/master/drivers/posdrv_linux/filter/x86_64/rastertopos

wget -q -O /home/pos/pos80.ppd \
  https://gitlab.com/ukitforce/pos-printers/-/raw/master/drivers/posdrv_linux/ppd/pos80.ppd


# Configure CUPS:
# --------------

# Edit /etc/cups/cupsd.conf and add user pos to all "Require user" lines. (no need because the next step adds pos to system group anyway).

# Add group "pos" to SystemGroup in the /etc/cups/cups-files.conf file
# SystemGroup sys root wheel pos



# Set printer margins, etc in firefox profile file.

# Download the printer driver and ppd file.


# /etc/gdm/custom.conf
# Open /etc/gdm/custom.conf and uncomment the line:
# WaylandEnable=false

# Note: Auto DNS registration script will not be installed.
#  See: https://gitlab.com/ukitforce/pos-provisioner/-/issues/3


# Disable GNOME welcome screen:
# https://www.putorius.net/disable-gnome-initial-setup.html

%end
